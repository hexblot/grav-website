---
title: Newsletter
---

# Newsletter

Χρησιμοποιήστε την παρακάτω φόρμα για να εγγραφείτε στο newsletter της Ελληνικής κοινότητας Drupal. Θα σας στέλνουμε ενημερώσεις σχετικά με meetups, drupalcamps και άλλα events που διοργανώνουμε στη χώρα μας. Ελέγξτε και το φάκελο ανεπιθύμητων (spam) μετά την εγγραφή σας!
_Για να ενημερώνεστε και να συμμετέχετε στα online meetups ελάτε στο [Slack](/slack)._

Αν αντιμετωπίζετε προβλήματα με τη φόρμα εγγραφής [χρησιμοποιήστε αυτό το σύνδεσμο](https://09895d1f.sibforms.com/serve/MUIEAAUyPyygQmlmNB5s7lASRdUk0rewF6S8-e0cgzCwqZdW0NrMo_VhmHq9XDoB-80pjIAuJ7gS4a_C9MZnmCulL8VwDcnSd6HsCWWid_5MDnMM4btLZhveQ2wR_kVSXIGXg5yfftrY3WbuBO4UtAiielKgIyCjCgDuwXOLE3WgxKN-yxHkoMEczxuFM27pIyedTD2GFwKKs2Sy).

<iframe width="540" height="660" src="https://09895d1f.sibforms.com/serve/MUIEAAUyPyygQmlmNB5s7lASRdUk0rewF6S8-e0cgzCwqZdW0NrMo_VhmHq9XDoB-80pjIAuJ7gS4a_C9MZnmCulL8VwDcnSd6HsCWWid_5MDnMM4btLZhveQ2wR_kVSXIGXg5yfftrY3WbuBO4UtAiielKgIyCjCgDuwXOLE3WgxKN-yxHkoMEczxuFM27pIyedTD2GFwKKs2Sy" frameborder="0" scrolling="auto" allowfullscreen style="display: block;margin-left: auto;margin-right: auto;max-width: 100%;"></iframe>
