---
title: 'Ελάτε στο Slack'
form:
    name: contact-form
    fields:
        -
            name: name
            label: Όνομα
            placeholder: 'Το ονομά σας'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                required: true
        -
            name: email
            label: Email
            placeholder: 'Το email σας'
            type: email
            validate:
                required: true
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            email:
                from: '{{ config.plugins.email.from }}'
                to:
                    - '{{ config.plugins.email.to }}'
                subject: '[Invite Request] {{ form.value.name|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Θα λάβετε την προσκλησή σας πολύ σύντομα!'
        -
            display: thankyou
---

# Βρείτε μας στο Slack

Συμπληρώστε τα στοιχεία σας ώστε να λάβετε invite για το Slack της Ελληνικής κοινότητας Drupal.

Αν αντιμετωπίζετε προβλήματα με την αυτόματη φόρμα μπορείτε να στείλετε email στο [slack@drupal.org.gr](mailto:slack@drupal.org.gr) και θα λάβετε το invite link.

*Δεν αποθηκεύουμε τα στοιχεία σας και ούτε θα σας σπαμάρουμε ποτέ!*
